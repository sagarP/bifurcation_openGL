from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from numpy import *
import sys
# Define the width and height variables as global
global width
global height
global x
global y
# Initial values for width and height
width = 600
height = 600
# Set the initial values of x and r
x = 0.5
r = 2.5

def init():
    glClearColor(1.0, 1.0, 1.0, 1.0)
    gluOrtho2D(-8.0,.0,0.0,1.0)

def plotLogistic(x,r):
    glPointSize(1.0)
    # Range over the entire interval
    for a in arange(-8.0, 7.0, 0.0001):
        # The logistic equation
        x = x*r*(1-x)
        glColor3f(0.0, 0.0, 1.0)
        glBegin(GL_POINTS)
        glVertex2f(a,x)
        glEnd()
        glFlush()
    # Print the final value
    print(f'The population stabilizes at x = {x}' )


def signals(key, x, y):
    # Allows us to quit by pressing 'Esc' or 'q'
    if key == chr(27):
        sys.exit()
    if key == "q":
        sys.exit()  
def draw():  
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT) # clear the screen
    plotLogistic(x,r)

def main():
    global width
    global height

    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGB|GLUT_SINGLE)
    glutInitWindowPosition(200,200)
    glutInitWindowSize(width,height)
    glutCreateWindow("Logistic Chaos")
    glutDisplayFunc(draw) # set callback for method draw
    glutKeyboardFunc(signals)

    init()

    glutMainLoop()


if __name__ == "__main__":
    x = float(input("Please Enter Initial Population Fractional size: xn = "))
    r = float(input("Please Enter the value of r: r = "))
    main()

# End Program