from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from numpy import *
import sys

# Define the width and height variables as global
global width
global height
# Initial values for width and height
width = 600
height = 600

def init():
    glClearColor(1.0, 1.0, 1.0, 1.0)
    gluOrtho2D(-8.0,7.0,0.0,1.0)

def plotBifurcation():
    glClear(GL_COLOR_BUFFER_BIT)

    glPointSize(1.0)
    x = 0.5
    r = 2.5

    for a in arange(-8.0, 7.0, 0.0001):
        r = r + 0.00001

        x = x*r*(1-x)
        glColor3f(1.0, 0.0, 0.0)
        glBegin(GL_POINTS)
        glVertex2f(a,x)
        glEnd()
        glFlush()

def signals(key, x, y):
    # Allows us to quit by pressing 'Esc' or 'q'
    if key == chr(27):
        sys.exit()
    if key == "q":
        sys.exit()  

def main():
    global width
    global height

    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGB|GLUT_SINGLE)
    glutInitWindowPosition(200,200)
    glutInitWindowSize(width,height)
    glutCreateWindow("Bifurcation Diagram | Chaos")
    glutDisplayFunc(plotBifurcation) # set callback for method plotBifurcation
    glutKeyboardFunc(signals)

    init()

    glutMainLoop()
main()
# End Program